import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {BASE_URL, fetchUsers} from '../src/utils/ApiTest';

describe('fetchUsers', () => {
  let mock;

  beforeAll(() => {
    mock = new MockAdapter(axios);
  });

  afterEach(() => {
    mock.reset();
  });

  describe('when API call is successful', () => {
    it('should return users todos', async () => {
      // given
      const todos = {
        results: [
          {id: '62309ca72a0ad93e1e948343', status: 'PENDING'},
          {id: '62309c7c15c6ee3e254e966c', status: 'PENDING'},
        ],
      };
      mock.onGet(`${BASE_URL}/todos`).reply(200, todos);

      // when
      const result = await fetchUsers();

      // then
      expect(result.data).toEqual(todos);
    });
  });

  describe('when API call fails', () => {
    it('should return empty todos todos', async () => {
      // given
      mock.onGet(`${BASE_URL}/todos`).networkErrorOnce();

      // when
      const result = await fetchUsers();

      // then
      expect(result).toEqual([]);
    });
  });
});
