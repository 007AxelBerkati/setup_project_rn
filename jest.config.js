// module.exports = {
//   preset: 'react-native',
//   moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
//   transformIgnorePatterns: [
//     "/node_modules/(?!react-native|react-clone-referenced-element|react-navigation)",
//     "/node_modules/(?!(jest-)?@?react-native|@react-native-community|@react-navigation)",
//     "/node_modules/(?!(@react-native|react-native)/).*/"
//   ]

// }


module.exports = {
  preset: 'react-native',
  testEnvironment: 'node',
  transform: {
    '^.+\\.ts?$': 'ts-jest',
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
  setupFilesAfterEnv: ["@testing-library/jest-native/extend-expect"]
};