import axios from 'axios';
import { LOGIN_USER } from '../../config';
import { showError, showSuccess } from '../../utils';
import {
  LOGIN_SUCCESS, LOGIN_FAILED, LOGIN_LOADING, LOGOUT,
} from '../types';

export const loginLoading = () => ({
  type: LOGIN_LOADING,
});

export const login = (token, name) => ({
  type: LOGIN_SUCCESS,
  payload: token,
  name,
});

export const logout = () => ({
  type: LOGOUT,
});

export const loginFailed = (data) => ({
  type: LOGIN_FAILED,
  payload: data,
});

export const loginUser = (email, password, navigation) => async (dispatch) => {
  dispatch(loginLoading());
  await axios.post(LOGIN_USER, { email, password })
    .then((response) => {
      if (response.data.tokens.access.token) {
        dispatch(login(response.data.tokens.access.token, response.data.user.name));
        navigation.replace('HomeScreen');
        showSuccess('Login Sukses');
      }
    }).catch((error) => {
      dispatch(loginFailed(error.response.data.message));
      showError(error.response.data.message);
    });
};
