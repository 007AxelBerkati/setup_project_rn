// Login
export const LOGIN_SUCCESS = '@LOGIN_SUCCESS';
export const LOGIN_FAILED = '@LOGIN_FAILED';
export const LOGIN_LOADING = '@LOGIN_LOADING';
export const LOGOUT = '@LOGOUT';

// Register
export const REGISTER_SUCCESS = '@REGISTER_SUCCESS';
export const REGISTER_FAILED = '@REGISTER_FAILED';
export const REGISTER_LOADING = '@REGISTER_LOADING';

// getData
export const GET_DATA_SUCCESS = '@GET_DATA_SUCCESS';
export const GET_DATA_FAILED = '@GET_DATA_FAILED';
export const GET_DATA_LOADING = '@GET_DATA_LOADING';

// getDetail
export const GET_DETAIL_SUCCESS = '@GET_DETAIL_SUCCESS';
export const GET_DETAIL_FAILED = '@GET_DETAIL_FAILED';
export const GET_DETAL_LOADING = '@GET_DETAL_LOADING';
