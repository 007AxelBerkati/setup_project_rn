import axios from 'axios';

export const BASE_URL = 'http://code.aldipee.com/api/v1';

export const fetchUsers = async () => {
  try {
    return await axios.get(`${BASE_URL}/todos`);
  } catch (e) {
    return [];
  }
};
