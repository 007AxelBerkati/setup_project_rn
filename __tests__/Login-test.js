import {loginUser} from '../src/redux/action';
import {loginReducer} from '../src/redux/reducer';

import configureMockStore from 'redux-mock-store';
import ReduxThunk from 'redux-thunk';
import nock from 'nock';
import MockAdapter from 'axios-mock-adapter';
import {LOGIN_SUCCESS} from '../src/redux/types';

const API_URL = 'http://code.aldipee.com/api/v1';
const middlewares = [ReduxThunk];
const mockStore = configureMockStore(middlewares);

describe('async actions', () => {
  let mock;

  beforeAll(() => {
    mock = new MockAdapter(axios);
  });

  afterEach(() => {
    mock.reset();
  });
  //     describe('when API call is successful', () => {
  //         it('should return users todos', async () => {
  //   it('creates AUTH_USER action when user is logged in', () => {
  //     // nock(API_URL)
  //     //   .post(/auth/login)
  //     //   .reply(200, { data: 'Logged in successfully'] }})

  //     const expectedActions = [
  //       { type: AUTH_USER }
  //     ]
  //     const store = mockStore({ })

  //     return store.dispatch(actions.loginUser({'example@x.com','password'}))
  //       .then(() => { // return of async actions
  //         expect(store.getActions()).toEqual(expectedActions)
  //       })
  //   })
  describe('when API login is successful', () => {
    it('should return users todos', async () => {
      mock
        .onPost(`${API_URL}/auth/login`, {
          email: 'cobacoba@gmail.com',
          password: 'cobacoba12',
        })
        .reply(200);

      const expectedActions = [{type: LOGIN_SUCCESS}];
      const store = mockStore({});

      return store
        .dispatch(actions.loginUser('cobacoba@gmail.com', 'cobacoba12'))
        .then(() => {
          // return of async actions
          expect(store.getActions()).toEqual(expectedActions);
        });
    });
  });

  //   it('creates AUTH_ERROR if user login fails', () => {
  //     nock(API_URL)
  //       .post(/auth/login)
  //       .reply(404, { data: {error: 404 }] }})

  //     const expectedActions = [
  //       { type: AUTH_ERROR }
  //     ]
  //     const store = mockStore({ })

  //     return store.dispatch(actions.loginUser({'example@x.com','password'}))
  //       .then(() => { // return of async actions
  //         expect(store.getActions()).toEqual(expectedActions)
  //       })
  //   })
  //   describe('when API call fails', () => {
  //     it('should return empty todos todos', async () => {
  //       // given
  //       mock.onGet(`${BASE_URL}/todos`).networkErrorOnce();

  //       // when
  //       const result = await fetchUsers();

  //       // then
  //       expect(result).toEqual([]);
  //     });
  //   });
});
