import AsyncStorage from '@react-native-async-storage/async-storage';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import reduxLogger from 'redux-logger';
import { persistReducer, persistStore } from 'redux-persist';
import createFilter from 'redux-persist-transform-filter';
import ReduxThunk from 'redux-thunk';
import { loginReducer, registerReducer } from '../reducer';

const saveSubsetFilter = createFilter('login', ['token', 'name']);

const persistConfig = {
  key: 'root',
  // blacklist: ['dataBooks'],
  whitelist: ['login'],
  storage: AsyncStorage,
  transform: [saveSubsetFilter],
};

const rootReducer = {
  login: loginReducer,
  register: registerReducer,
};

const configPersist = persistReducer(persistConfig, combineReducers(rootReducer));

export const Store = createStore(
  configPersist,
  applyMiddleware(ReduxThunk, reduxLogger),
);

export const Persistore = persistStore(Store);
