import axios from 'axios';
import { REGISTER_USER } from '../../config';
import { REGISTER_SUCCESS, REGISTER_FAILED, REGISTER_LOADING } from '../types';

export const registerSuccess = () => ({
  type: REGISTER_SUCCESS,
});

export const registerFailed = () => ({
  type: REGISTER_FAILED,
});

export const registerLoading = () => ({
  type: REGISTER_LOADING,
});

export const checkRegister = (name, email, password, navigation) => async (dispatch) => {
  dispatch(registerLoading());
  await axios
    .post(REGISTER_USER, { email, password, name })
    .then(() => {
      dispatch(registerSuccess());
      navigation.replace('SuccessRegister');
    })
    .catch((err) => {
      dispatch(registerFailed(err.response.data.message));
    });
};
