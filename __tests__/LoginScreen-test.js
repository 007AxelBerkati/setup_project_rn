import 'react-native'
import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import {shallow, configure} from 'enzyme'
import renderer from 'react-test-renderer'
import { render, fireEvent } from '@testing-library/react-native';
import {LoginScreen} from '../src/pages'

configure({adapter: new Adapter(), disableLifecycleMethods: true})

const loginWrapper = shallow(<LoginScreen />)
 
jest.mock('@react-navigation/native', () => ({
  useNavigation: component => component,
}))
 
 
describe('LoginScreen Screen', () => {
  it('should renders correctly', () => {
    renderer.create(<LoginScreen />)
  })
 
  it('should renders `LoginScreen Screen` module correctly', () => {
    expect(loginWrapper).toMatchSnapshot()
  })
 
  describe('Check component', () => {
    it('should create find `Input`', () => {
      expect(loginWrapper.find('Input').exists())
    })
 
    it('should create `TouchableOpacity` component', () => {
      expect(loginWrapper.find('TouchableOpacity').exists())
    })
    it('should create `LinkComponent` component', () => {
      expect(loginWrapper.find('LinkComponent').exists())
    })
 
    describe('Mount component', () => {
      describe('Event Test', () => {
        it('should change text when `Input` Change', () => {
          const mockCallBack = jest.fn()
          loginWrapper.find('[testID="input-email"]').simulate('change')
          expect(mockCallBack.mock.calls.length)
        })

        it('should change text when `Input` Change', () => {
          const mockCallBack = jest.fn()
          loginWrapper.find('[testID="input-password"]').simulate('change')
          expect(mockCallBack.mock.calls.length)
        })
 
        it('should click when `login` Pressed', () => {
          const mockCallBack = jest.fn()
          loginWrapper.find('[testID="btn-login"]').simulate('press')
          expect(mockCallBack.mock.calls.length)
        })
       

        it('should navigate when `register` Pressed', () => {
          const navigation = { navigate: () => {} }
          spyOn(navigation, 'navigate')
          const page = render(<LoginScreen navigation={navigation} />)
          const registerButton = page.getByTestId('btn-register')

          fireEvent.press(registerButton)

          expect(navigation.navigate).toHaveBeenCalledWith('HomeScreen')
        })
      })
    })
  })
})