import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import FlashMessage from 'react-native-flash-message';
import {
  SplashScreen, HomeScreen,
} from '../pages';
import LoginScreen from '../pages/LoginScreen';

const Stack = createNativeStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator initialRouteName="SplashScreen">
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}

function Router() {
  return (
    <>
      <NavigationContainer>
        <MyStack />
      </NavigationContainer>
      <FlashMessage position="top" />
    </>

  );
}

export default Router;
