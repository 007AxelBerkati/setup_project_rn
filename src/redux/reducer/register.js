import {
  REGISTER_FAILED, REGISTER_LOADING, REGISTER_SUCCESS,
} from '../types';

const initialRegisterState = {
  isLoading: false,
  errorMessage: '',
};

// eslint-disable-next-line default-param-last
export const registerReducer = (state = initialRegisterState, action) => {
  switch (action.type) {
    case REGISTER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        errorMessage: '',
      };
    case REGISTER_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    case REGISTER_FAILED:
      return {
        ...state,
        errorMessage: action.payload,
        isLoading: false,
      };

    default:
      return state;
  }
};
