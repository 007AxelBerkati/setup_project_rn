const mainColors = {
  black1: '#17082A',
  black2: '#000000',
  black3: 'rgba(33, 15, 55, 1)',
  black3Opacity: 'rgba(33, 15, 55, 0.85)',
  black4: '#112340',
  purple1: '#6644B8',
  orange1: '#F79E44',
  pink1: '#FF8FC6',
  pink2: '#ECBBDA',
  white2: 'rgba(255,255,255, 0.2)',
  grey1: '#7D8797',
  grey2: '#E9E9E9',
  grey3: '#EDEEF0',
  grey4: '#B1B7C2',
  green1: '#0BCAD4',
  green2: '#0AA8B0',
  red1: '#E06379',
};

export const colors = {
  warning: mainColors.red1,
  white: 'white',
  background: {
    primary: 'white',
    secondary: mainColors.green1,
    icon: mainColors.green2,
    genre: mainColors.white2,
  },

  text: {
    primary: mainColors.black4,
    secondary: mainColors.grey1,
    rating: mainColors.orange1,
    genre: mainColors.pink2,
    description: mainColors.grey4,
  },
  button: {
    primary: {
      background: mainColors.purple1,
      text: 'white',
    },
    secondary: {
      background: mainColors.pink1,
      text: 'white',
    },
  },

};
