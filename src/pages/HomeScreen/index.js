import {
  StyleSheet, Text, TouchableOpacity, View,
} from 'react-native';
import React from 'react';
import { useDispatch } from 'react-redux';
import { logout } from '../../redux/action';

function HomeScreen() {
  const dispatch = useDispatch();

  return (
    <View style={styles.page}>
      <TouchableOpacity onPress={() => dispatch(logout())}>
        <Text>HomeScreen</Text>
      </TouchableOpacity>
    </View>
  );
}

export default HomeScreen;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
