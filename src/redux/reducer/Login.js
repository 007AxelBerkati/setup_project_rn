import {
  LOGIN_SUCCESS, LOGIN_FAILED, LOGIN_LOADING, LOGOUT,
} from '../types';

const initialLoginState = {
  token: '',
  name: '',
  isLoading: false,
  errorMessage: '',
};

// eslint-disable-next-line default-param-last
export const loginReducer = (state = initialLoginState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        token: action.payload,
        name: action.name,
        isLoading: false,
        errorMessage: '',
      };
    case LOGIN_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    case LOGIN_FAILED:
      return {
        ...state,
        errorMessage: action.payload,
        isLoading: false,
      };

    case LOGOUT:
      return {
        ...state,
        token: '',
        name: '',
      };

    default:
      return state;
  }
};
