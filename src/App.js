import NetInfo from '@react-native-community/netinfo';
import React, { useEffect, useState } from 'react';
import { SafeAreaView } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { NoInternet } from './component';
import { Persistore, Store } from './redux/store';
import Router from './router';

function AppWrapper() {
  const [isOnline, setIsOnline] = useState(false);

  const removeNetInfo = () => {
    NetInfo.addEventListener((state) => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setIsOnline(offline);
    });
  };

  useEffect(() => {
    removeNetInfo();
    return () => {
      removeNetInfo();
    };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      {
      isOnline ? <NoInternet /> : <Router />
      }
    </SafeAreaView>
  );
}

function App() {
  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={Persistore}>
        <AppWrapper />
      </PersistGate>
    </Provider>
  );
}

export default App;
